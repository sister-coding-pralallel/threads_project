#import os, requests, threading, time, urllib.request, urllib.error, urllib.parse
import os
import requests
import threading
import urllib.request, urllib.error, urllib.parse
import time

#definisikan url yang ingin di download filenya 
url = "https://apod.nasa.gov/apod/image/1901/LOmbradellaTerraFinazzi.jpg"

#fungsi untuk membangun range byte
def buildRange(value, numsplits):
    #buat list untuk menyimpan hasil rangenya 
    lst = []
    #loop untuk membuat range 
    for i in range(numsplits):
        #kondisi jika i = 0 
        if i == 0:
            #append byte
            lst.append('%s-%s' % (i, int(round(1 + i * value/(numsplits*1.0) + value/(numsplits*1.0)-1, 0))))
        #kondisi jika i selain 0 
        else:
            #append byte
            lst.append('%s-%s' % (int(round(1 + i * value/(numsplits*1.0),0)), int(round(1 + i * value/(numsplits*1.0) + value/(numsplits*1.0)-1, 0))))
    #return list 
    return lst

#kelas untuk split thread
class SplitBufferThreads(threading.Thread):
    """ Splits the buffer to ny number of threads
        thereby, concurrently downloading through
        ny number of threads.
    """
    #fungsi untuk mendefinisikan self
    def __init__(self, url, byteRange):
        super(SplitBufferThreads, self).__init__()
        #definisikan self url
        self.__url = url
        #definisikan byte range
        self.__byteRange = byteRange
        #definisikan self request
        self.req = None
    #fungsi untuk request range byte
    def run(self):
        #definisikan self request yang berisi request range 
        self.req = urllib.request.Request(self.__url,  headers={'Range': 'bytes=%s' % self.__byteRange})
    
    #buat fungsi untuk mendapatkan file data
    def getFileData(self):
        #return request using urllib
        return urllib.request.urlopen(self.req).read()

#fungsi main yang sudah di split menjadi 3 thread
def main(url=None, splitBy=3):
    #start time untuk menetahui waktu awal downloading
    start_time = time.time()
    #kodisi jika url tidak ada
    if not url:
        #print notifikasi jika url tidak ada
        print("Please Enter some url to begin download.")
        #return
        return
    #split url menggunakan '/' pada index ke -1
    fileName = url.split('/')[-1]
    #request part yang akan di encoding
    sizeInBytes = requests.head(url, headers={'Accept-Encoding': 'identity'}).headers.get('content-length', None)
    #print notifikasi bytes yang akan di download 
    print("%s bytes to download." % sizeInBytes)
    #kondisi jika bukan size in bytes 
    if not sizeInBytes:
        #print notifikasi Size cannot be determined. 
        print("Size cannot be determined.")
        #return
        return
    #bagi pekerjaan kebeberapa thread dengan looping
    dataLst = []
    for idx in range(splitBy):
        #bagi pekerjaan ke thread satu pada build range index ke idx
        byteRange = buildRange(int(sizeInBytes), splitBy)[idx]
        bufTh = SplitBufferThreads(url, byteRange)
        #start pekerjaan di semua thread serentak
        bufTh.start()
        #join semua hasil pekerjaan daru tiap thread
        bufTh.join()
        #append ke list dan get file data 
        dataLst.append(bufTh.getFileData())
        
    #definisikan konten yang sudah disatukan/join 
    content = b''.join(dataLst)
    if dataLst:
        #kondisi jika file sudah ada
        if os.path.exists(fileName):
            #jika sudak ada file di hapus
            os.remove(fileName)
        print("--- %s seconds ---" % str(time.time() - start_time))
        #buat file untuk menulis hasil file yang di download
        with open(fileName, 'wb') as fh:
            #tulis file
            fh.write(content)
        #buat notifikasi jika file sudah di write 
        print("Finished Writing file %s" % fileName)
#panggil main 
if __name__ == '__main__':
    main(url)